package com.teklifver.Controller;

import com.teklifver.Services.*;
import com.teklifver.data.AddressData;
import com.teklifver.data.UserData;
import com.teklifver.entity.*;
import com.teklifver.form.CompanyRegisterForm;
import com.teklifver.form.IndividualRegisterForm;
import com.teklifver.form.PostForm;
import com.teklifver.repository.CityRepository;
import com.teklifver.repository.PostRepository;
import com.teklifver.repository.TownRepository;
import com.teklifver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(name = "register")
public class RegisterPageController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @Autowired
    private TownService townService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TownRepository townRepository;

    @Autowired
    private PostRepository postRepository;




    @RequestMapping("/individual")
    public String getRegisterPage(Model model)
    {
        List<CityEntity> list=new ArrayList<>();
         Iterable<CityEntity>  cityEntities=cityRepository.findAll();
        for (CityEntity p:cityEntities)
        {
            list.add(p);
        }
        model.addAttribute("popUp",true);
        model.addAttribute("cities",list);
        model.addAttribute("individualRegisterForm", new IndividualRegisterForm());
        return "registerCompany";
    }

    @RequestMapping(value = "/saveIndividual", method = RequestMethod.POST)
    public String saveIndividual(@Valid IndividualRegisterForm individualRegisterForm,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes,
                                 HttpServletRequest request,Model model)
    {
       if (bindingResult.hasErrors())
       {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.IndividualRegisterForm", bindingResult);
            Iterable<TownEntity> townEntities = townRepository.findAll();
            Iterable<CityEntity> cityEntities = cityRepository.findAll();
            model.addAttribute("cities",cityEntities);
            model.addAttribute("towns",townEntities);
            return "pages/personalRegister";
        }
     else {

            String email = individualRegisterForm.getMail();
            UserEntity userEntity = userRepository.findUserEntityByEmail(email);
            if (userEntity != null)
            {
                return "redirect:/personalRegister";
            }
            AddressData addressData = new AddressData();
            populateAddressData(addressData,individualRegisterForm);

            addressData = addressService.save(addressData);

            UserData userData = new UserData();
            userData.setAddress_id(addressData.getAddressId());
            populateUserData(userData,individualRegisterForm);
            HttpSession session = request.getSession();
            userData = userService.save(userData);
            session.setAttribute("customer",userData);
            redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Siparis guncellendi"));
            return "redirect:/";
        }

    }
    @RequestMapping(value = "postShow/{id}",method = RequestMethod.GET)
    public String showProduct(@PathVariable Long id, Model model,HttpServletRequest request){
        PostEntity postEntity = postService.getPostEntity(id);
        PostEntity post = postRepository.findOne(id);
        UserEntity user = userRepository.findOne(Long.parseLong(postEntity.getUserId()));
        UserData userData = userService.isLogin(request);
        if (userData != null){
            UserEntity userEntity = userRepository.findOne(Long.parseLong(userData.getUserId()));
            model.addAttribute("user",userEntity);
        }
        else{
            model.addAttribute("user",user);
        }
        if (userData != null){
         if (post.getUserId().equals(userData.getUserId())){
             model.addAttribute("control",true);
         }
         else{
             model.addAttribute("control",false);
         }
        }
        else{
            model.addAttribute("control", null);
        }
        Iterable<PostEntity> postEntities  = postRepository.findAll();
        model.addAttribute("posts", postEntities);

        model.addAttribute("post",postEntity);
        model.addAttribute("postForm",new PostForm());
        return "pages/advisertmentInformation";
    }

    @RequestMapping(value = "/saveCompany",method = RequestMethod.POST)
    public String saveCompany(@Valid @ModelAttribute CompanyRegisterForm companyRegisterForm,BindingResult bindingResult,  HttpServletRequest request)
    {

        if (!bindingResult.hasErrors()){
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setCategoryId(companyRegisterForm.getCategoryId());
        companyEntity.setCompanyName(companyRegisterForm.getCompanyName());
        companyEntity.setEmail(companyRegisterForm.getMail());
        companyEntity.setPhoneNumber(companyRegisterForm.getPhoneNumber());
        companyEntity.setSubCategoryId(companyRegisterForm.getSubCategoryId());
        companyEntity.setCityId(companyRegisterForm.getCity());
        companyEntity.setTownId(companyRegisterForm.getTown());
        companyEntity.setPassword(companyRegisterForm.getPassword());
        companyService.save(companyEntity);
        UserData userData = new UserData();

        userData.setName(companyEntity.getCompanyName());
        userData.setEmail(companyEntity.getEmail());
        userData.setPhoneNumber(companyEntity.getPhoneNumber());
        userService.save(userData);
        HttpSession session = request.getSession();
        session.setAttribute("customer",userData);
            return "redirect:/";
        }
        else {
            return "pages/companyRegister";
        }

    }

    private void populateAddressData(AddressData addressData , IndividualRegisterForm individualRegisterForm)
    {
        addressData.setCityName(individualRegisterForm.getCity());
        addressData.setTownName(individualRegisterForm.getTown());
        addressData.setLine(individualRegisterForm.getLine());
    }

    private void populateUserData(UserData userData, IndividualRegisterForm individualRegisterForm)
    {
        userData.setEmail(individualRegisterForm.getMail());
        userData.setLastName(individualRegisterForm.getLastName());
        userData.setName(individualRegisterForm.getName());
        userData.setPhoneNumber(individualRegisterForm.getPhoneNumber());
        userData.setPassword(individualRegisterForm.getPassword());
        userData.setCityName(individualRegisterForm.getCity());
        userData.setTownName(individualRegisterForm.getTown());
        userData.setAddressLine(individualRegisterForm.getLine());
    }

//    @GetMapping("/town/{provinceId}")
//    @ResponseBody
//    public List<TownEntity> getTown(@PathVariable int provinceId)
//    {
//        return townService.getTownByProvinceId(provinceId);
//    }
//
//    @GetMapping("/district/{townId}")
//    @ResponseBody
//    public List<DistrictEntity> getDistrict(@PathVariable int townId)
//    {
//        return districtService.getDistrictByTownId(Integer.toString(townId));
//    }

    @RequestMapping("/corporate")
    public void registerCorparatePage(Model model)
    {

    }




}
