package com.teklifver.Services;


import com.teklifver.entity.SubCategoryEntity;
import com.teklifver.form.SubCategoryForm;
import com.teklifver.repository.SubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;


    public List<SubCategoryEntity> getSubCategoryByCategoryId(int categoryId)
    {
        return subCategoryRepository.getSubCategoryEntitiesByCategoryId(Integer.toString(categoryId));
    }

    public void editSubCategory(SubCategoryForm subCategoryForm , SubCategoryEntity subCategoryEntity ){
            subCategoryEntity.setSubCategoryName(subCategoryForm.getSubCategoryName());
            subCategoryRepository.save(subCategoryEntity);
    }
    public  SubCategoryEntity findOne(Long id){
        return  subCategoryRepository.findOne(id);
    }
    public SubCategoryEntity getSubCategoryBySubCategoryName(String subCategoryName){
        SubCategoryEntity subCategoryEntity = subCategoryRepository.findBySubCategoryName(subCategoryName);
        return subCategoryEntity;
    }
    public void saveSubCategory(SubCategoryForm subCategoryForm){
        if(!subCategoryForm.getSubCategoryName().isEmpty()){
            SubCategoryEntity subCategoryEntity = new SubCategoryEntity();
            subCategoryEntity.setCategoryName(subCategoryForm.getCategoryName());
            subCategoryEntity.setSubCategoryName(subCategoryForm.getSubCategoryName());
            subCategoryRepository.save(subCategoryEntity);
        }
    }
    public  void delete(Long id){
        SubCategoryEntity subCategoryEntity = subCategoryRepository.findOne(id);
        subCategoryRepository.delete(subCategoryEntity);
    }

    public Iterable<SubCategoryEntity> findAll(){
        return subCategoryRepository.findAll();
    }
}
