package com.teklifver.form;

/**
 * Created by hasan on 2.04.2018.
 */
public class LoginForm {
    String mail;
    String password;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
