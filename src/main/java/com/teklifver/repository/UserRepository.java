package com.teklifver.repository;

/**
 * Created by hasan on 17.04.2018.
 */

import com.teklifver.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity,Long> {
    UserEntity findAllByEmail(String eMail);
    UserEntity findUserEntityByEmail(String email);

}
