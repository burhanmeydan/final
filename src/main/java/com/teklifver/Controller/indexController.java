package com.teklifver.Controller;

/**
 * Created by hasan on 10/29/2017.
 */

import com.teklifver.Services.PostService;
import com.teklifver.Services.SubCategoryService;
import com.teklifver.Services.UserService;
import com.teklifver.data.UserData;
import com.teklifver.entity.*;
import com.teklifver.form.SirketUyeForm;
import com.teklifver.form.UserUyeForm;
import com.teklifver.repository.AddressRepository;
import com.teklifver.repository.CategoryRepository;
import com.teklifver.repository.CityRepository;
import com.teklifver.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class indexController {

    @Autowired
    CityRepository cityRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostService postService;

    @Autowired
    private SubCategoryService subCategoryService;

    @GetMapping
    public String gotoIndex(HttpServletRequest request,Model model)
    {
        Boolean isLogin = false;
        if ((userService.isLogin(request) != null))
        {
            isLogin = true;
            model.addAttribute("customerData",userService.isLogin(request));
        }
        model.addAttribute("isLogin",isLogin);
        Iterable<CategoryEntity> categoryEntityIterable = categoryRepository.findAll();
        List<CategoryEntity> categoryEntities = new ArrayList<>();


        for (CategoryEntity categoryEntity : categoryEntityIterable)
        {
            categoryEntities.add(categoryEntity);
        }

        Iterable<PostEntity> postEntities = (List<PostEntity>) postRepository.findAll();

        model.addAttribute("posts",postEntities);
        model.addAttribute("categories",categoryEntities);
        return "index2";
    }

    @GetMapping("/logOut")
    public String logOut(Model model,HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        session.setAttribute("customer",null);

        return "redirect:/";
    }

    @GetMapping("/category/{categoryId")
    public String getPost(@PathVariable("categoryId") int categoryId, RedirectAttributes redirectAttributes)
    {
        List<PostEntity> postEntities = postService.getPostByCateGoryId(Integer.toString(categoryId));
        redirectAttributes.addFlashAttribute("posts",postEntities);
        List<SubCategoryEntity> subCategoryEntities = subCategoryService.getSubCategoryByCategoryId(categoryId);
        redirectAttributes.addFlashAttribute("subCategories",subCategoryEntities);
        return "redirect:/";
    }



//    @RequestMapping("/")
//    public String goruntule(Model model,HttpServletRequest request){
//        HttpSession session=request.getSession();
//        request.getSession().getAttribute("loginorexit");
//        session.setAttribute("loginorexit","GirisYap");
//        String login=(String)request.getSession().getAttribute("loginorexit");
//        model.addAttribute("logins",login);
//        model.addAttribute("isim",session.getAttribute("username"));
//        return "index2";
//    }
//    @RequestMapping("/login")
//    public  String login(Model model)
//    {
//        model.addAttribute("loginForm",new LoginForm());
//        return "Login";
//    }
    @RequestMapping("/Uye")
    public String Uye(Model model)
    {
        model.addAttribute("useruyeform", new UserUyeForm());
        model.addAttribute("sirketuyeform",new SirketUyeForm());
        return "UyeOl";
    }
//    @RequestMapping("/bireysel")
//    public String Bireysel(Model model)
//    {
//        List<Province> list=new ArrayList<>();
//        Iterable<Province>  provinces=provinceReporsitory.findAll();
//        for (Province p:provinces)
//        {
//            list.add(p);
//        }
//        model.addAttribute("provinces",list);
//        model.addAttribute("useruyeform", new UserUyeForm());
//        return "register";
//    }
//    @RequestMapping("/kurumsal")
//    public String Kurumsal(Model model)
//    {
//        List<Province> list=new ArrayList<>();
//        Iterable<Province>  provinces=provinceReporsitory.findAll();
//        for (Province p:provinces)
//        {
//            list.add(p);
//        }
//        model.addAttribute("provinces",list);
//        model.addAttribute("sirketuyeform", new SirketUyeForm());
//        return "UyeSirket";
//    }
//    @RequestMapping("/ilanver")
//   public  String ilanver(Model model)
//    {
//        model.addAttribute("ilanform",new IlanForm());
//        return "/ilanver";
//    }





}
