package com.teklifver.Services;

import com.teklifver.data.UserData;
import com.teklifver.entity.UserEntity;
import com.teklifver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;



    public UserData save(UserData userData)
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setAddress_id(userData.getAddress_id());
        userEntity.setEmail(userData.getEmail());
        userEntity.setLastName(userData.getLastName());
        userEntity.setName(userData.getName());
        userEntity.setPassword(userData.getPassword());
        userEntity.setPhoneNumber(userData.getPhoneNumber());
        userEntity.setCityName(userData.getCityName());
        userEntity.setTownName(userData.getTownName());
        userEntity.setAddressLine(userData.getAddressLine());

        userRepository.save(userEntity);
        userData.setUserId(userEntity.getId().toString());
        return userData;
    }

    public UserData isLogin(HttpServletRequest request)
    {
        HttpSession session = request.getSession();

        if (session.getAttribute("customer") != null)
        {
            UserData userData = (UserData) session.getAttribute("customer");
            return userData;
        }

        return null;
    }

    public UserData populateUserDat(UserEntity userEntity)
    {
        UserData userData = new UserData();
        userData.setUserId(userEntity.getId().toString());
        userData.setPhoneNumber(userEntity.getPhoneNumber());
        userData.setEmail(userEntity.getEmail());
        userData.setName(userEntity.getName());
        userData.setLastName(userEntity.getLastName());
        userData.setAddress_id(userEntity.getAddress_id());
        return userData;
    }

    public UserEntity findOne(Long id) {
        return userRepository.findOne(id);
    }


}
